//"Tarea 1"
// EJERCICIO 1:
var info = {
    name: "Christian",
    lastname: "Cueva",
    nickname: "Aladino",
  };

  //console.log(`Me llamo ${nombre} ${lastname} y me dicen ${nickname}`)
  /* 
    Crear una funcion que reciba 3 parametros (name, lastname y nickname) 
    El resultado debe mostrar por consola el texto "Me llamo Christian Cueva y me dicen Aladino"
  */
 //Ejercicio 1 Hecho
    var nombre = 'Christian';
    var apellido = "Cueva";
    var apodo = "Aladino";
    
    var mensaje2 = `Me llamo ${nombre} ${apellido} y me dicen ${apodo}`;

    console.log(mensaje2);

    // EJERCICIO 2 Hecho 
var info = {
    name: "Clark",
    lastname: "Kent",
    nickname: "Superman",
  };
  var user = {
    name: info.name,
    lastname: info.lastname,
    nickname: info.nickname,
    allPowers: ["power: Superfuerza",
               "power: Volar",
               "power: Velocidad",
               "power: Telepatia"],
  };
  var user= {
    info,
    ...user
    };
    
    console.log(user)
  /* 
    Crear una funcion de tipo promesa que reciba el arreglo de poderes y devuelva un nuevo formato
    Como resultado hacer un console.log(user) debe mostrar lo siguiente.
    {
      name: 'Clark',
      lastname: 'Kent',
      allPowers: [
        { power: 'Superfuerza' },
        { power: 'Volar' },
        { power: 'Velocidad' },
        { power: 'Telepatia' },
      ]
    }
    */
  
// EJERCICIO 3
/* 
  Crear una clase llamada Figura con propiedades base y altura
  Los valores de base y altura deben ser inicializadas a traves del constructor
  La clase debe tener un metodo llamado getAreaTriangulo, esta funcion debera calcular el area de un triangulo
*/

//Ejercicio 3 Hecho 

let figura=prompt('Figura con propiedades base y altura')
let base;
let altura;
let area; 
switch (figura) {
  case 'triángulo':
  base=prompt('Ingresa la base del triángulo');
  altura=prompt('Ingresa la altura del triangulo');
  area=base*altura/2;
  
  console.log('El area del triangulo es:'+area );
  break;
  
  default:
    console.log('opcion no valida!');
  break;


}

// EJERCICIO 4
var datos = [
  { name: "Kelvin", age: 31, country: "Perú" },
  { name: "Milagros", age: 26, country: "Colombia" },
  { name: "Luis", age: 28, country: "Colombia" },
  { name: "Ruben", age: 29, country: "Brasil" },
  { name: "Andrea", age: 29, country: "Argentina" },
];

/*
  Resultados
  1. Mostrar el objeto que tenga como propiedad edad 29
    { name: "Ruben", age: 29, country: "Brasil" }
  2. Mostrar en un arreglo, todas los objetos que tenga como propiedad edad 29
    [
      { name: "Ruben", age: 29, country: "Brasil" },
      { name: "Andrea", age: 29, country: "Argentina" },
    ]
  3. Mostrar un arreglo de solo edades => 
    [31,26,28,29,29]
  4. Del arreglo del punto 3, mostrar por consola si existe o no el numero 30
    false
*/
//Ejercicio 4 resuelto 
const datos = [
  { name: "Kelvin", age: 31, country: "Perú" },
  { name: "Milagros", age: 26, country: "Colombia" },
  { name: "Luis", age: 28, country: "Colombia" },
  { name: "Ruben", age: 29, country: "Brasil" },
  { name: "Andrea", age: 29, country: "Argentina" },
];

const resultado = datos.find( alguien => alguien.age == 29 );
console.log(resultado)


const resultado2 = datos.filter( alguien => alguien.age == 29 );
console.log(resultado2)

var doubles = datos.map(function(x) {
  return x.age;});
  console.log(doubles);

let incluyetreinta = doubles.includes(30);

console.log(incluyetreinta) 






